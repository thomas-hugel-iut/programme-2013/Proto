package iut;

import io.vertx.rxjava.core.AbstractVerticle;
import io.vertx.rxjava.core.eventbus.Message;

import io.vertx.rxjava.core.net.NetServer;
import io.vertx.rxjava.core.net.NetSocket;
import io.vertx.rxjava.core.http.HttpServer;
import io.vertx.rxjava.core.http.HttpServerRequest;

public class PongServer extends AbstractVerticle {

  private static final String PING_PONG_TABLE = "ping-pong-table";
  private static final String PONG = "pong";

  @Override
  public void start() {
    super.vertx.eventBus().consumer(PING_PONG_TABLE, this::handleRequest);
  }

  private void handleRequest (Message<Object> message) {
    message.reply(PONG);
  }
/*
  @Override
  public void start() {
    // NetServer tcpServer = super.vertx.createNetServer();
    // tcpServer.connectHandler((NetSocket socket) -> {
    //   socket.handler(buffer -> {
    //     System.out.println("I received some bytes: " + buffer.length());
    //   });
    // });
    // tcpServer.listen(12345);

    HttpServer server = vertx.createHttpServer();
    // server.requestStream().toObservable().subscribe((HttpServerRequest req) ->
    //   req.response().end("Hello from " + Thread.currentThread().getName()));
  
    server.requestStream().toObservable().subscribe(this::handleRequest);
  
    server.listen(8080);
  }

  private void handleRequest(HttpServerRequest req) {
    req.response().end("Hello from " + Thread.currentThread().getName());
  }
*/
}
