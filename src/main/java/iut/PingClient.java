package iut;

import io.vertx.rxjava.core.AbstractVerticle;

public class PingClient extends AbstractVerticle {

  private static final String PING_PONG_TABLE = "ping-pong-table";
  private static final String PING = "ping";

  @Override
  public void start() {
    vertx.eventBus().send(PING_PONG_TABLE, "ping", reply -> {
      if (reply.succeeded()) {
        System.out.println("Received: " + reply.result().body());
      } else {
        // No reply or failure
        reply.cause().printStackTrace();
      }
    });
  }

}
